# Gohugo Issues Log


I would update this page when necessary to record issues and solutions for GoHugo maintenance.

## Display Errors

issue 1: 

Index page invalid posts

Within the path `/content/posts/` there is none of a post, or all Header Metadata has set to: `draft: true`.

issue 2

Current environment is "development". The "comment system", "CDN" and "fingerprint" will be disabled.

issue 3

Draft post could be display although I've set it to `draft: true` in header metadata.
