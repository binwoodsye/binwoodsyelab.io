---
title: Hello Gohugo - a blog to grab values
date: 2021-06-01
draft: false
---

Initially I tented to name this post *expressive writing*, however there was a dilemma on this title. 

The expressive context could be anything, which includes negative feelings; these appears to be about depression, hater, terror, and eroticism etc. Yet by humanity concerns, one should expose these primitively mental activities within a safe environment, such as writing down on a private diary, or telling stories in a clinic.

In the other hand, for expressive writing everyone might have positive thoughts. For instance,  they could have experiences tackling web blog generation - like Gohugo blog on Github here, or they may validate values they've read from an article. All these could help themselves and someone else - those are values.
 
Once there's value, we may use writing to pursue it. A quotation has inspired me at this point
	

{{< admonition quote "ZHU Xiaochao says" true >}}
Writing could cure
{{< /admonition >}}



said ZHU Xiaochao, a psychology therapist who is giving lectures on CBT - Cognitive behavioral therapy. According to him, thoughts of an individual could be generally irrational or arbitrary. He also says writing could fix this issue, as discourse markers are likely to connect ideas in better coherence and cohesion. By writing, people might be aware of their thoughts.

I do see examples of writing could cure. In the episode of BBC Sherlock Holmes (2010), following the guidance of his therapist, John Watson started to write his feelings and experiences during his mission to Afghanistan, which had shattered him into PTSD.

But writing to me could be more than remedy. I notice it could be my way of creation beyond libido. This is the libido far more than three child policy. Apparently I might find my happiness by writing, once if I could grab the value around me.